export enum GenderEnum {
    male = 'male',
    female = 'female',
}

export enum RoleEnum {
    clinicAdmin = 'clinicAdmin',
    superAdmin = 'superAdmin',
    otherStaff = 'otherStaff'
}

export enum staffEnum {
    doctor = 'doctor',
    nurse = 'nurse',
    other = 'other',
    management  = 'management '
}
export interface ClinicInterface {
    name: string,
    serialNumber: 0,
    email: string,
    address1: string,
    address2: string,
    plan: string,
    emailVerified: false,
    phoneNumber: string,
    phoneNumberVerified: false,
    orgnizations: [string],
    verifiedPublicHealth: [],
    profile: string,
    paypal: false,
    organization: string,
    admin: string
}

export interface UserInterface {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    phoneNumber: string,
    gender: GenderEnum,
    address1: string,
    address2: string,
    emailVerified: boolean,
    phoneNumberVerified: boolean,
    profile: string,
    roles: RoleEnum,
    licence: string,
    licenceExpiry: string,
    staff: staffEnum[],
    clinic: ClinicInterface,
    orgnizations: [string],
    needPasswordChange: boolean,
    authToken: string
}
