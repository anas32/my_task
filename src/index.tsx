import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '../src/assests/css/main.css'
import '../src/assests/css/login.css'
import '../src/assests/css/header.css'
import '../src/assests/css/sidenav.css'
import '../src/assests/css/create-clinic.css'
import '../src/assests/css/subsribtion-modal.css'
import '../src/assests/css/clinics-list.css'
import '../src/assests/css/organization.css'
import '../src/assests/css/chat.css'
import '../src/assests/css/new-organization.css'
import '../src/assests/css/not-found.css'
import '../src/assests/css/del-organization.css'

import { Provider } from 'react-redux';
import {store} from './redux'
import { AuthContextProvider } from './app/context/Auth';
import App from './app/App';

import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <AuthContextProvider>
              <App />
          </AuthContextProvider>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
