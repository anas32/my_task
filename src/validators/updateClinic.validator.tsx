import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator';
import {Transform} from "class-transformer";
export class updateClinicValidator {
    @IsString()
    @IsNotEmpty()
    name: string | undefined;

    // @ts-ignore
    @IsEmail({ message: 'Invalid Email' })
    @IsNotEmpty()
    email: string | undefined;


    // @ts-ignore
    @IsPhoneNumber(null, { message: 'Phone Number must be valid' })
    @IsNotEmpty()
    @Transform(({ value }) => {
        return value.startsWith('+') ? value : '+' + value;
    })
    phoneNumber: string | undefined;

    @IsString()
    @IsNotEmpty()
    plan: string | undefined;


    @IsString()
    @IsNotEmpty()
    address1: string | undefined;
}
