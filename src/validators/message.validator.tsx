import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator';

export class MessageValidator {
    @IsString()
    @IsNotEmpty()
    message: string;
}
