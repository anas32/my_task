import {IsNotEmpty} from 'class-validator';

export class CreateOrganizationValidator {
    @IsNotEmpty()
    name: string | undefined;


    @IsNotEmpty()
    description: string | undefined;
}
