import { IsEmail, IsNotEmpty} from 'class-validator';

export class LoginValidator {
    @IsEmail()
    @IsNotEmpty()
    identifier: string | undefined;

    @IsNotEmpty()
    password: string | undefined;
}
