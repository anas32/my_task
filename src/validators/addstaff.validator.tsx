import {IsEmail, IsNotEmpty, IsPhoneNumber, IsString} from 'class-validator';
import {Transform} from "class-transformer";

export class AddstaffValidator {
    @IsString()
    @IsNotEmpty({message: 'First Name should not be empty'})
    firstName: string | undefined;

    @IsString()
    @IsNotEmpty()
    lastName: string | undefined;

    @IsEmail()
    @IsNotEmpty()
    email: string | undefined;

    // @ts-ignore
    @IsPhoneNumber(null, { message: 'Phone Number must be valid' })
    @IsNotEmpty({ message: 'Phone Number should not be empty' })
    @Transform(({ value }) => {
        return value.startsWith('+') ? value : '+' + value;
    })
    phoneNumber: string | undefined;

    @IsString()
    @IsNotEmpty()
    licence: string | undefined;

    @IsNotEmpty()
    profile: string | undefined;


    @IsNotEmpty()
    password: string | undefined;

    @IsString({ message: 'Experience must be valid' })
    @IsNotEmpty({ message: 'Experience should not be empty' })
    staff: string | undefined;
}
