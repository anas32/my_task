import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator';
import {Transform} from "class-transformer";
import { Match } from './custom-class-validator';

export class CreateClinicValidator {
    @IsString()
    @IsNotEmpty()
    name: string | undefined;

    // @ts-ignore
    @IsEmail({ message: 'Invalid Email' })
    @IsNotEmpty()
    email: string | undefined;


    // @ts-ignore
    @IsPhoneNumber(null, { message: 'Phone Number must be valid' })
    @IsNotEmpty()
    @Transform(({ value }) => {
        return value.startsWith('+') ? value : '+' + value;
    })
    phoneNumber: string | undefined;

    @IsString()
    @IsNotEmpty()
    plan: string | undefined;


    @IsString()
    @IsNotEmpty()
    address1: string | undefined;

    @IsString()
    @IsNotEmpty()
    organization: string | undefined;

    @IsString({ message: 'Admin First Name must be String' })
    @IsNotEmpty({ message: 'Admin First Name should not be empty' })
    adminFirstName: string | undefined

    @IsString({ message: 'Admin Last Name must be String' })
    @IsNotEmpty({ message: 'Admin Last Name should not be empty' })
    adminLastName: string | undefined

    // @ts-ignore
    @IsEmail({ message: 'Invalid Email' })
    @IsNotEmpty({ message: 'Admin Email should not be empty' })
    adminEmail: string | undefined


    // @ts-ignore
    @IsPhoneNumber( null, { message: 'Admin Phone Number must be valid' })
    @IsNotEmpty({ message: 'Admin Phone Number should not be empty' })
    @Transform(({ value }) => {
        return value.startsWith('+') ? value : '+' + value;
    })
    adminPh: string | undefined;

    @IsString()
    @IsNotEmpty()
    password: string | undefined;

    @IsString({ message: 'Confirm Password must be valid' })
    @IsNotEmpty()
    @Match('password', {
        message: 'Password and Confirm Password Must Be The Same',
    })
    confirmPassword: string | undefined;
}
