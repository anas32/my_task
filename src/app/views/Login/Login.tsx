import React, {useContext, useState} from 'react'
import {useHistory} from 'react-router-dom'
import medicareImage from '../../../assests/images/medical_care_movn.png'
import logo from '../../../assests/images/logo.png'
import { ErrorMessage, Form, Formik, FormikHelpers } from 'formik';
import visibility from '../../../assests/icons/visibility.svg';
import visibilityOff from '../../../assests/icons/visibility_off.svg';
import {FormikConfig} from "formik/dist/types";
import {FormikValidator} from "../../../utility/FormikValidator";
import {LoginValidator} from "../../../validators/login.validator";
import axios from '../../axios'
import {AuthContext} from "../../context/Auth";

const Login = (): JSX.Element => {
    const authContext = useContext(AuthContext)
    const history = useHistory();

    const [passwordVisibility, setPasswordVisibility] = useState(false)
    const changePasswordVisibility = () => {
        setPasswordVisibility(!passwordVisibility);
    }

    const formik: FormikConfig<LoginValidator> = {
        initialValues: {
            identifier: '',
            password: '',
        },
        onSubmit(values: LoginValidator, formikHelpers: FormikHelpers<any>): void | Promise<any> {
            axios
                .post('/auth/login', values)
                .then((response) => {
                    authContext.actions.updateState(response.data);
                    console.log("login =>", response.data);
                    history.push('/main-layout')
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        validate: FormikValidator.validator(LoginValidator),
    };


    return (
        <div className='login-page'>
            <div className="image-side">
                <img src={medicareImage} alt=""/>
            </div>
            <div className="login-side">
                <div className="login">
                    <div className="logo">
                        <img src={logo} alt=""/>
                        <p>Log in to go to your account.</p>
                    </div>
                    <Formik {...formik}>
                        {(formikProps) => (
                            <Form>
                                <div className="form-check">
                                    <label htmlFor="identifier">E-mail</label>
                                    <input
                                        type="email"
                                        id='identifier'
                                        name='identifier'
                                        className='form-control'
                                        placeholder='user@mail.com'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="identifier"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check mt-3">
                                    <label htmlFor="password">Password</label>
                                    <div className="form-control-password">
                                        <input
                                            type={passwordVisibility ? "text" : "password"}
                                            id='password'
                                            name='password'
                                            placeholder='Password'
                                            onChange={formikProps.handleChange}
                                        />
                                        {!passwordVisibility && (
                                            <img src={visibilityOff} onClick={changePasswordVisibility} alt=""/>
                                        )}
                                        {passwordVisibility && (
                                            <img src={visibility} onClick={changePasswordVisibility} alt=""/>
                                        )}
                                    </div>
                                    <ErrorMessage
                                        name="password"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <button type='submit' className='btn'>Log In</button>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>

        </div>
    )
}

export default Login
