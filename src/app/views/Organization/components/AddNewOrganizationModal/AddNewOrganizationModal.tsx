import React from 'react'
import {Modal} from "react-bootstrap";
import {ErrorMessage, Form, Formik, FormikHelpers} from "formik";
import {FormikConfig} from "formik/dist/types";
import {FormikValidator} from "../../../../../utility/FormikValidator";
import {CreateOrganizationValidator} from "../../../../../validators/createOrganization.validator";
import axios from "../../../../axios/axios";

const AddNewOrganizationModal = (props: any): JSX.Element => {
    const handleClose = () => props.setShow(false);

    const formik: FormikConfig<CreateOrganizationValidator> = {
        initialValues: {
            name: '',
            description: '',
        },
        onSubmit(values: CreateOrganizationValidator, formikHelpers: FormikHelpers<any>): void | Promise<any> {
            axios
                .post('/organization/create', values)
                .then((response) => {
                    console.log(response.data);
                    handleClose();
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        validate: FormikValidator.validator(CreateOrganizationValidator),
    };

    return (
        <div>
            <Modal show={props.show} onHide={handleClose} centered>
                <div className="new-organization">
                    <Formik {...formik}>
                        {(formikProps) => (
                            <Form>
                                <span className='remove-modal' onClick={handleClose}>
                                    <span className="material-icons">clear</span>
                                </span>
                                <h1>Create Organization</h1>
                                <div className="form-check">
                                    <label htmlFor="name">Name</label>
                                    <input
                                        type="text"
                                        id='name'
                                        name='name'
                                        className='form-control'
                                        placeholder='Clinic Name'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="name"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check mt-3">
                                    <label htmlFor="description">Description</label>
                                    <textarea
                                        id='description'
                                        name='description'
                                        className='form-control'
                                        placeholder='Description'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="description"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="submit-btn">
                                    <button className="btn" type='submit'>Create Organization</button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </Modal>
        </div>
    )
}

export default AddNewOrganizationModal

