import React from 'react'
import {Modal} from "react-bootstrap";
import axios from "../../../../axios/axios";

const DelOrganizationModal = (props: any): JSX.Element => {
    const handleShowDelModal = () => props.setShow(false);
    const handleDelOrganization = () => {
        axios
            .delete('/organization')
            .then((response) => {
                handleShowDelModal();
            })
            .catch((error) => {
                console.error(error);
            });
    }


        return (
        <div>
            <Modal show={props.show} onHide={handleShowDelModal} centered>
                <div className="del-organization">
                    <h1>Are You Sure You Want To Delete This Organization</h1>
                    <div className="del-organization-btns">
                        <button type='button' className='btn' onClick={handleShowDelModal}>Cancel</button>
                        <button type='button' className='btn confrim' onClick={handleDelOrganization}>Confirm</button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default DelOrganizationModal
