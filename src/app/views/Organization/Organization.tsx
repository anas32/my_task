import React, {useEffect, useState} from 'react'
import {Dropdown} from "react-bootstrap";
import morebtn from '../../../assests/icons/more.svg'
import AddNewOrganizationModal from "./components/AddNewOrganizationModal/AddNewOrganizationModal";
import axios from "../../axios/axios";
import DelOrganizationModal from "./components/DelOrganizationModal";

const ClinicsList = (): JSX.Element => {
    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);

    const [showDelModal, setShowDelModal] = useState(false);
    const handleShowDelModal = () => setShowDelModal(true);


    const [organizations, setOrganization] = useState<any>([]);
    useEffect(() => {
        axios.get('/organization/all')
            .then((response) => {
                setOrganization(response.data)
            })
            .catch((error: any) => {
                console.error(error);
            });
    }, []);

    return (
        <div className='Organization list'>
            <div className="organization-btn">
                <button type='button' className='btn' onClick={handleShow}>Create New Organization</button>
            </div>
            <table>
                <tr>
                    <th className='p-3'>No</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Doctor</th>
                    <th>Clinics</th>
                    <th>Others</th>
                    <th/>
                </tr>
                {organizations.map((item: any) => (
                    <tr className='list'>
                        <td className='p-3'>1</td>
                        <td>{item.name}</td>
                        <td>{item.description}</td>
                        <td>{item.clinics}</td>
                        <td>{item.doctors}</td>
                        <td>{item.others}</td>
                        <td>
                            <Dropdown>
                                <Dropdown.Toggle className='dropdown-btn' variant="success" id="dropdown-basic">
                                    <img src={morebtn} alt=""/>
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item className='dropdown-item' href="#">
                                        <span className="material-icons">local_hospital</span>
                                        Add Doctors
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item' href="#">
                                        <span className="material-icons">home</span>
                                        Add Clinics
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item mb-1' href="#">
                                        <span className="material-icons">person_add</span>
                                        Add Managing Staff
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item dropdown-edit pt-2' href="">
                                        <span className="material-icons">edit</span>
                                        Edit
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item dropdown-del' onClick={handleShowDelModal}>
                                        <span className="material-icons">delete</span>
                                        Delete
                                    </Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </td>
                    </tr>
                ))}
            </table>
            <AddNewOrganizationModal show={show} setShow={setShow}/>
            <DelOrganizationModal show={showDelModal} setShow={setShowDelModal} />
        </div>
    )
}

export default ClinicsList
