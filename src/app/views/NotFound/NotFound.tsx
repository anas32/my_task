import React from 'react'
import {Link} from 'react-router-dom'
import notFound from '../../../assests/images/not_found.png'

const NotFound = ():JSX.Element => {

    return (
        <div className='not-found'>
            <img src={notFound} alt=""/>
            <p>Page Not Found</p>
            <Link to='/main-layout'><button className="btn" type='button'>Back To Home</button></Link>
        </div>
    )
}
export default NotFound
