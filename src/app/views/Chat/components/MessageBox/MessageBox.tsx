import React, {useContext, useEffect, useState} from "react";
import {useMessageContext} from "../../../../context/Messages";
import {Form, Formik, FormikHelpers} from "formik";
import axios from "../../../../axios/axios";
import {useHistory} from "react-router-dom";
import {FormikConfig} from "formik/dist/types";
import {MessageValidator} from "../../../../../validators/message.validator";
import {FormikValidator} from "../../../../../utility/FormikValidator";
import {AuthContext} from "../../../../context/Auth";

interface Props {

}

export function MessageBox (props: Props):JSX.Element {
    const messageContext = useMessageContext();

    const history = useHistory()
    const authContext = useContext(AuthContext);

    const [messages, setMessages] = useState([]);

    useEffect(() => {
        if(messageContext.state.message){
            const newMessages = [...messages]
            newMessages.push(messageContext.state.message)
            setMessages(newMessages)
        }
    }, [messageContext.state])


    const formik: FormikConfig<MessageValidator> = {
        initialValues: {
            message: '',
        },
        onSubmit(values: MessageValidator, formikHelpers: FormikHelpers<any>): void | Promise<any> {

            const searchParams = new URLSearchParams(history.location.search)
            const room = searchParams.get('room')
            axios
                .post('/Chat/message', {...values,room, messageBy: authContext.state.user.id}
                )
                .then((response) => {
                    // console.log(response.data);
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        validate: FormikValidator.validator(MessageValidator),
    };

    useEffect(() => {
        const searchParams = new URLSearchParams(history.location.search)
        if(searchParams.has('room')){
            const room = searchParams.get('room')
            axios.get('/Chat/Messages', {
                params: {
                    room
                }
            })
                .then((response) => {
                    console.log("Messages =>", response.data)
                    setMessages(response.data)
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }, [history.location]);

    return <>
        <div className="chat-container">
            <div className="chat-header">
                <p>Chat header</p>
            </div>
            <div className="chat-messages">
                {messages.map(item => (
                    <div className="message">
                        <div className="message-text">
                            <p>{item.message}</p>
                            <p className='createdAt'>{item.createdAt}</p>
                        </div>
                        <div className="message-icon">
                            <span className="material-icons">account_circle</span>
                        </div>
                    </div>
                ))}
            </div>
            <Formik {...formik}>
                {(formikProps) => (
                    <Form>
                        <div className="form-check">
                            <input
                                type="text"
                                name='message'
                                className='form-control'
                                onChange={formikProps.handleChange}
                                placeholder='Type Massage...'
                            />
                            <button className='btn' type='submit'><span className="material-icons">send</span>
                            </button>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    </>
}
