import React, {useContext, useEffect, useState} from 'react';
import axios from "../../axios/axios";
import {AuthContext} from "../../context/Auth";
import {Link} from "react-router-dom";
import {MessagesContextProvider} from "../../context/Messages";
import {MessageBox} from "./components";

const Chat = (): JSX.Element => {
    const authContext = useContext(AuthContext);
    const [chatRooms, setChatRooms] = useState([]);

    useEffect(() => {
        const memberId = authContext.state.user.id;
        axios.get('/Chat/rooms', {
            params: {
                member: memberId
            }
        })
            .then((response) => {
                // console.log("Rooms =>", response.data)
                setChatRooms(response.data)
            })
            .catch((error) => {
                console.error(error);
            });
    }, []);




    return (
        <div className='chat-section'>
            <MessagesContextProvider>
                <h3>Chat</h3>
                <div className="chat-containers">
                    <div className="users">
                        <div className="search-bar">
                            <input type="text" placeholder="Search"/>
                            <button><span className="material-icons">search</span></button>
                        </div>
                        <div className="grps-section">
                            <h1>Groups</h1>
                            {chatRooms.map((item) => (
                                <Link to={{
                                    pathname: "/main-layout/chat",
                                    search: `?room=${item.id}`
                                }} className="grp">
                                    <div className="grp-img">
                                        <span className="material-icons">account_circle</span>
                                    </div>
                                    <div className="grp-title">
                                        <h2>{item.title}</h2>
                                        <p>No Massage Sent</p>
                                    </div>
                                    <div className="grp-desc">
                                        {/*<p className='m-0 mb-1'>Today</p>*/}
                                        {/*<p className="color">2</p>*/}
                                    </div>
                                </Link>
                            ))}
                        </div>
                        <div className="chats">
                            <h1>Chat</h1>
                            <div className="grps">
                                <div className="grps">
                                    <div className="grp">
                                        <div className="grp-img">
                                            {/*<span className="material-icons">account_circle</span>*/}
                                        </div>
                                        <div className="grp-title">
                                            <h2></h2>
                                            <p></p>
                                        </div>
                                        <div className="grp-desc">
                                            <p className='m-0 mb-1'></p>
                                            <p className="color"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="create-grp">
                            <button className='btn' type='button'><span className="material-icons">group_add</span></button>
                        </div>
                    </div>
                    <MessageBox />
                </div>
            </MessagesContextProvider>

        </div>
    )
}

export default Chat
