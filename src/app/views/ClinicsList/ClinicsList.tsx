import React, {useState, useEffect} from 'react'
import {Dropdown} from "react-bootstrap";
import more from '../../../assests/icons/more.svg'
import axios from '../../axios'
import DelClinicModal from "./components/DelClinicModal/DelClinicModal";

const ClinicsList = (): JSX.Element => {
    const [showDelModal, setShowDelModal] = useState(false);
    const handleShowDelModal = () => setShowDelModal(true);

    const [clinics, setClinics] = useState<any>([]);

    useEffect(() => {
        axios.get('/clinic/all')
            .then((response) => {
                setClinics(response.data)
            })
            .catch((error: any) => {
                console.error(error);
            });
    }, []);
    return (
        <div className='clinics list'>
            <table>
                <tr>
                    <th className='p-2'>No</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th></th>
                </tr>


                {clinics.map((clinic: any) => (
                    <tr className='list'>
                        <td className='p-2'>{clinic.serialNumber}</td>
                        <td>{clinic.name}</td>
                        <td>{clinic.address1}</td>
                        <td>{clinic.email}</td>
                        <td>{clinic.phoneNumber}</td>
                        <td>
                            <Dropdown>
                                <Dropdown.Toggle className='dropdown-btn' variant="success" id="dropdown-basic">
                                    <img src={more} alt=""/>
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item className='dropdown-item dropdown-add-staff' href={`/main-layout/add-staff/${clinic.id}`}>
                                        <span className="material-icons">person_add</span>
                                        Add Staff
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item' href="#">
                                        <span className="material-icons">list</span>
                                        Staff List
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item mb-1' href="#">
                                        <span className="material-icons">home</span>
                                        Public Health
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item dropdown-edit pt-2' href={`/main-layout/edit-clinic/${clinic.id}`}>
                                        <span className="material-icons">edit</span>
                                        Edit
                                    </Dropdown.Item>
                                    <Dropdown.Item className='dropdown-item dropdown-del' onClick={handleShowDelModal}>
                                        <span className="material-icons">delete</span>
                                        Delete
                                    </Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </td>
                    </tr>
                ))}
            </table>
            <DelClinicModal show={showDelModal} setShow={setShowDelModal}/>
        </div>
    )
}

export default ClinicsList
