import React from 'react'
import {Modal} from "react-bootstrap";

const DelClinicModal = (props: any): JSX.Element => {
    const handleShowDelModal = () => props.setShow(false);

    return (
        <div>
            <Modal show={props.show} onHide={handleShowDelModal} centered>
                <div className="del-organization">
                    <h1>Are You Sure You Want To Delete This Clicnic</h1>
                    <div className="del-organization-btns">
                        <button type='button' className='btn' onClick={handleShowDelModal}>Cancel</button>
                        <button type='button' className='btn confrim'>Confirm</button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default DelClinicModal
