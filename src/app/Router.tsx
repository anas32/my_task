import React from 'react';
import {Redirect, Route, Switch } from 'react-router-dom';
import Login from "./views/Login";
import MainLayout from "./layouts/MainLayout";
import NotFound from "./views/NotFound";

export function Router(): JSX.Element {
    return (
        <Switch>
            <Route component={MainLayout} path="/main-layout" />
            <Route component={Login} path="/login" />
            <Route component={NotFound} path="/404"/>
            <Redirect to="/login" />
        </Switch>
    );
}
