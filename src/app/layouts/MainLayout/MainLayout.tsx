import React from 'react';
import Header from '../../components/Header';
import SideNav from "../../components/SideNav";
import { Router } from './Router';

const MainLayout = (): JSX.Element => {
    return (
        <div className="wrapper">
                <SideNav />
            <div className="main-content">
                <Header />
                <Router />
            </div>
        </div>
    );
};
export default MainLayout;
