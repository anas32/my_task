import React from 'react'
import {Redirect, Route, Switch} from 'react-router-dom';
import Dashboard from "../../views/Dashboard";
import ClinicsList from '../../views/ClinicsList';
import Subscription from "../../views/Subscription";
import Organization from '../../views/Organization';
import Settings from "../../views/Settings";
import Chat from "../../views/Chat";
import CreateClinic from "../../components/CreateClinic";
import UpdateClinic from "../../components/UpdateClinic";
import AddStaff from "../../components/AddStaff";

export function Router(): JSX.Element {
    return (
        <Switch>
            <Route component={Dashboard} path='/main-layout/dashboard'/>
            <Route component={ClinicsList} path='/main-layout/clinics'/>
            <Route component={Subscription} path='/main-layout/subscription'/>
            <Route component={Chat} path='/main-layout/chat'/>
            <Route component={Organization} path='/main-layout/Organization'/>
            <Route component={Settings} path='/main-layout/settings'/>
            <Route component={CreateClinic} path='/main-layout/create-clinic'/>
            <Route component={UpdateClinic} path='/main-layout/edit-clinic/:id'/>
            <Route component={AddStaff} path='/main-layout/add-staff/:id'/>
            <Redirect to="/main-layout/dashboard"/>
        </Switch>
    )
}

