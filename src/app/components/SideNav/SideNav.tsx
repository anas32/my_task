import React from 'react'
import logo from '../../../assests/images/logo.png';
import circlePlus from '../../../assests/icons/add-circle.svg';
import {Link, NavLink} from "react-router-dom";

const SideNav = (): JSX.Element => {
    return (
        <div className='sidenav'>
            <div className="logo">
                <img src={logo} alt=""/>
            </div>
            <div className="nav-btns">
                <Link to='/main-layout/create-clinic'>
                    <button type='button' className='btn'>
                        <img src={circlePlus} alt=""/>
                        Add New Clinic
                    </button>
                </Link>
            </div>
            <ul className="nav-links">
                <li><NavLink to='/main-layout/dashboard'>
                    <span className="material-icons">dashboard</span>
                    Dashboard
                </NavLink></li>
                <li><NavLink to='/main-layout/clinics'>
                    <span className="material-icons">home</span>
                    Clinics
                </NavLink></li>
                <li><NavLink to='/main-layout/subscription'>
                    <span className="material-icons">subscriptions</span>
                    Subscription
                </NavLink></li>
                <li><NavLink to='/main-layout/Organization'>
                    <span className="material-icons">groups</span>
                    Organization
                </NavLink></li>
                <li><NavLink to='/main-layout/chat'>
                    <span className="material-icons">chat</span>
                    Chat
                </NavLink></li>
                <li><NavLink to='/main-layout/settings'>
                    <span className="material-icons">settings</span>
                    Settings
                </NavLink></li>
            </ul>
        </div>
    )
}

export default SideNav
