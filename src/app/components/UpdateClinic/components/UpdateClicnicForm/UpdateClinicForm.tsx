import React, {useEffect, useState} from 'react'
import {ErrorMessage, Form, FormikProps} from "formik";
import axios from "../../../../axios/axios";
import {updateClinicValidator} from "../../../../../validators/updateClinic.validator";

interface Props {
    formikProps: FormikProps<updateClinicValidator>;
    clinic: string;
}
const UpdateClinicForm = (props: Props):JSX.Element => {
    const formikProps = props.formikProps;
    const [organizations, setOrganization] = useState<any>([]);
    useEffect(() => {
        axios.get('/organization/all')
            .then((response) => {
                setOrganization(response.data)
            })
            .catch((error: any) => {
                console.error(error);
            });
    }, []);

    useEffect(() => {
        axios.get('/clinic/byId', {
            params: {
                id : props.clinic
            }
        })
            .then((response) => {
                formikProps.setValues({
                    ...response.data
                })
            })
            .catch((error: any) => {
                console.error(error);
            });
    }, []);

    return (
        <Form>
            <h1>Clinic Details</h1>
            <div className="form-checks">
                <div className="form-check">
                    <label htmlFor="name">Clinic Name</label>
                    <input
                        type="text"
                        id='name'
                        name='name'
                        className='form-control'
                        placeholder='Clinic Name'
                        value={formikProps.values.name}
                        onChange={formikProps.handleChange}
                    />
                    <ErrorMessage
                        name="name"
                        render={(error) => <span className="error">{error}</span>}
                    />
                </div>
                <div className="form-check">
                    <label htmlFor="email">Clinic email</label>
                    <input
                        type="email"
                        id='email'
                        name='email'
                        className='form-control'
                        placeholder='Clinic Email'
                        value={formikProps.values.email}
                        onChange={formikProps.handleChange}
                        onBlur={formikProps.handleBlur}
                    />
                    <ErrorMessage
                        name="email"
                        render={(error) => <span className="error">{error}</span>}
                    />
                </div>
            </div>
            <div className="form-checks">
                <div className="form-check">
                    <label htmlFor="phoneNumber">Phone Number</label>
                    <input
                        type="text"
                        id='phoneNumber'
                        name='phoneNumber'
                        className='form-control'
                        placeholder='Phone Number'
                        value={formikProps.values.phoneNumber}
                        onChange={formikProps.handleChange}
                        onBlur={formikProps.handleBlur}
                    />
                    <ErrorMessage
                        name="phoneNumber"
                        render={(error) => <span className="error">{error}</span>}
                    />
                </div>
                <div className="form-check">
                    <label htmlFor="plan">Subscription Fee Monthly Fee Monthly</label>
                    <input
                        type="text"
                        id='plan'
                        name='plan'
                        className='form-control'
                        placeholder='Subscription Fee Monthly'
                        value={formikProps.values.plan}
                        onChange={formikProps.handleChange}
                        onBlur={formikProps.handleBlur}
                    />
                    <ErrorMessage
                        name="plan"
                        render={(error) => <span className="error">{error}</span>}
                    />
                </div>
            </div>
            <div className="form-checks">
                <div className="form-check">
                    <label htmlFor="address1">Clinic Address Line 1</label>
                    <input
                        type="text"
                        id='address1'
                        name='address1'
                        className='form-control'
                        placeholder='Clinic Address Line 1'
                        value={formikProps.values.address1}
                        onChange={formikProps.handleChange}
                        onBlur={formikProps.handleBlur}
                    />
                    <ErrorMessage
                        name="address1"
                        render={(error) => <span className="error">{error}</span>}
                    />
                </div>
                <div className="form-check">
                    <label htmlFor="address2">Clinic Address Line 2 <span className='same-color'>(Optional)</span></label>
                    <input
                        type="text"
                        id='address2'
                        name='address2'
                        className='form-control'
                        placeholder='Clinic Address Line 2 (Optional)'
                        onChange={formikProps.handleChange}
                        onBlur={formikProps.handleBlur}
                    />
                    <ErrorMessage
                        name="address2"
                        render={(error) => <span className="error">{error}</span>}
                    />
                </div>
            </div>
            <div className="form-check mt-3">
                <label htmlFor="organization">Organizations<span className='same-color'>(Optional)</span></label>
                <select
                    name='organization'
                    className='form-select'
                    id='organization'
                >
                    {organizations.map((item: any) => (
                        <option value={item.id}>{item.name}</option>
                    ))}
                </select>
            </div>
            <div className="submit-btn">
                <button className="btn" type='submit'>Save Changes</button>
            </div>
        </Form>
    )
}

export default UpdateClinicForm
