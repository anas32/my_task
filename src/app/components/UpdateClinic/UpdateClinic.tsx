﻿import React, {useEffect, useState} from 'react'
import { RouteComponentProps} from 'react-router'
import { ErrorMessage, Form, Formik, FormikHelpers } from 'formik';
import {FormikConfig} from "formik/dist/types";
import {CreateClinicValidator} from "../../../validators/createClinic.validator";
import {FormikValidator} from "../../../utility/FormikValidator";
import UpdateClinicForm from "./components/UpdateClicnicForm/UpdateClinicForm";
import axios from "../../axios/axios";
import {updateClinicValidator} from "../../../validators/updateClinic.validator";

const UpdateClinic = (props: RouteComponentProps<{id: string}>): JSX.Element => {
    const formik: FormikConfig<updateClinicValidator> = {
        initialValues: {
            name: '',
            email: '',
            phoneNumber: '',
            plan: '',
            address1: '',
        },
        onSubmit(values: updateClinicValidator, formikHelpers: FormikHelpers<any>): void | Promise<any> {
            const clinicId = props.match?.params?.id;
            axios
                .patch('/clinic', {...values}, {
                    params: {
                        id: clinicId
                    }
                })
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        validate: FormikValidator.validator(updateClinicValidator),
    };
    return (
        <div className='create-clinic-section'>
            <div className='create-clinic content'>
                <Formik {...formik}>
                    {(formikProps) => (
                        <UpdateClinicForm key="update-clinic" formikProps={formikProps} clinic={props.match.params.id} />
                    )}
                </Formik>
            </div>
        </div>
    )
}

export default UpdateClinic
