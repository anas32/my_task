import React from 'react'
import {Dropdown} from "react-bootstrap";

const Header = (): JSX.Element => {
    return (
        <header>
            <div className="title">
                <h1></h1>
            </div>
            <div className="user-section">
                <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic" className='dropdown-btn'>
                        <span className="material-icons">account_circle</span>
                        <p>Super Admin</p>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item className='dropdown-item' href="#/action-1">
                            <span className="material-icons">settings</span>
                            Settings
                        </Dropdown.Item>
                        <Dropdown.Item className='dropdown-item' href="#/action-2">
                         <span className="material-icons">lock</span>
                            Change Password
                        </Dropdown.Item>
                        <Dropdown.Item className='dropdown-item' href="/login">
                            <span className="material-icons">logout</span>
                            Logout
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </div>
        </header>
    )
}

export default Header
