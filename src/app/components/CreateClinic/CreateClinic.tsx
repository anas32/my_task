import React, {useEffect, useState} from 'react'
import {CreateClinicValidator} from "../../../validators/createClinic.validator";
import {FormikConfig} from 'formik/dist/types';
import {FormikValidator} from "../../../utility/FormikValidator";
import { ErrorMessage, Form, Formik, FormikHelpers } from 'formik';
import PaymentModal from "./components";
import axios from "../../axios/axios";

const CreateClinic = (): JSX.Element => {
    const [show, setShow] = useState(false);

    const handleShow = () => setShow(true);

    const formik: FormikConfig<CreateClinicValidator> = {
        initialValues: {
            name: '',
            email: '',
            phoneNumber: '',
            plan: '',
            address1: '',
            organization: '',
            adminFirstName: '',
            adminLastName: '',
            adminEmail: '',
            adminPh: '',
            password: '',
            confirmPassword: '',
        },
        onSubmit(values: CreateClinicValidator, formikHelpers: FormikHelpers<any>): void | Promise<any> {
            axios
                .post('/clinic/create', values)
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        validate: FormikValidator.validator(CreateClinicValidator),
    };


    const [organizations, setOrganization] = useState<any>([]);
    useEffect(() => {
        axios.get('/organization/all')
            .then((response) => {
                setOrganization(response.data)
            })
            .catch((error: any) => {
                console.error(error);
            });
    }, []);

    return (
        <div className='create-clinic-section'>
            <h3>Add New Clinic</h3>
            <div className='create-clinic content'>
                <Formik {...formik}>
                    {(formikProps) => (
                        <Form>
                            <h1>Clinic Details</h1>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="name">Clinic Name</label>
                                    <input
                                        type="text"
                                        id='name'
                                        name='name'
                                        className='form-control'
                                        placeholder='Clinic Name'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="name"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="email">Clinic email</label>
                                    <input
                                        type="email"
                                        id='email'
                                        name='email'
                                        className='form-control'
                                        placeholder='Clinic Email'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="email"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="phoneNumber">Phone Number</label>
                                    <input
                                        type="text"
                                        id='phoneNumber'
                                        name='phoneNumber'
                                        className='form-control'
                                        placeholder='Phone Number'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="phoneNumber"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="plan">Subscription Fee Monthly Fee Monthly</label>
                                   <div className="form-control different-form-check">
                                       <input
                                           type="text"
                                           id='plan'
                                           name='plan'
                                           className='different-input'
                                           placeholder='Subscription Fee Monthly'
                                           onChange={formikProps.handleChange}
                                       />
                                       <button className='btn' type='button' onClick={handleShow}>Chose Plan</button>
                                   </div>
                                    <ErrorMessage
                                        name="plan"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="address1">Clinic Address Line 1</label>
                                    <input
                                        type="text"
                                        id='address1'
                                        name='address1'
                                        className='form-control'
                                        placeholder='Clinic Address Line 1'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="address1"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="address2">Clinic Address Line 2 <span
                                        className='same-color'>(Optional)</span></label>
                                    <input
                                        type="text"
                                        id='address2'
                                        name='address2'
                                        className='form-control'
                                        placeholder='Clinic Address Line 2 (Optional)'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="address2"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="organization">Organizations</label>
                                    <select
                                        name='organization'
                                        className='form-select'
                                        onChange={formikProps.handleChange}
                                        id='organization'
                                    >
                                     {organizations.map((item: any) => (
                                         <option value={item.id}>{item.name}</option>
                                     ))}
                                    </select>
                                    <ErrorMessage
                                        name="organization"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check"/>
                            </div>
                            <h2>Clinical Admin Detail</h2>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="adminFirstName">Admin First Name</label>
                                    <input
                                        type="text"
                                        id='adminFirstName'
                                        name='adminFirstName'
                                        className='form-control'
                                        placeholder='Admin First Name'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="adminFirstName"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="adminLastName">Admin Last Name</label>
                                    <input
                                        type="text"
                                        id='adminLastName'
                                        name='adminLastName'
                                        className='form-control'
                                        placeholder='Admin Last Email'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="adminLastName"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="adminEmail">Admin Email</label>
                                    <input
                                        type="email"
                                        id='adminEmail'
                                        name='adminEmail'
                                        className='form-control'
                                        placeholder='Admin Password'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="adminEmail"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="adminPh">Admin Phone Number</label>
                                    <input
                                        type="text"
                                        id='adminPh'
                                        name='adminPh'
                                        className='form-control'
                                        placeholder='Admin Phone Number'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="adminPh"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        id='password'
                                        name='password'
                                        className='form-control'
                                        placeholder='Password'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="password"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="confirmPassword">Confirm Password</label>
                                    <input
                                        type="Password"
                                        id='confirmPassword'
                                        name='confirmPassword'
                                        className='form-control'
                                        placeholder='Confirm Password'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="confirmPassword"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="submit-btn">
                                <button className="btn" type='submit'>Add Clinic</button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
            <PaymentModal show={show} setShow={setShow}/>
        </div>
    )
}

export default CreateClinic
