import React from 'react'
import {Modal} from "react-bootstrap";

const PaymentModal = (props: any):JSX.Element => {
    const handleClose = () => props.setShow(false);
    return (
        <div>
            <Modal show={props.show} onHide={handleClose} centered>
                <div className="subscription-modal">
                    <div className="plan">
                        <h1>Basic</h1>
                        <h2>$5.99 <span>/Month</span> </h2>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                    <div className="modal-button">
                        <button type='button' className='btn'>Get Started</button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default PaymentModal
