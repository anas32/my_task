import React, {ChangeEvent} from 'react'
import { ErrorMessage, Form, Formik, FormikHelpers } from 'formik';
import {FormikConfig} from "formik/dist/types";
import {FormikValidator} from "../../../utility/FormikValidator";
import {AddstaffValidator} from "../../../validators/addstaff.validator";
import axios from "../../axios/axios";
import {RouteComponentProps} from "react-router";

const AddStaff = (props: RouteComponentProps<{id: string}>): JSX.Element => {
    const formik: FormikConfig<AddstaffValidator> = {
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            licence: '',
            profile: '',
            password: '',
            staff: '',
        },
        onSubmit(values: AddstaffValidator, formikHelpers: FormikHelpers<any>): void | Promise<any> {
            const clinicId = props.match?.params?.id;
            axios
                .post('/clinic/addStaff', {...values, staff: [values.staff]}, {
                    params: {
                        clinicId: clinicId
                    }
                })
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        validate: FormikValidator.validator(AddstaffValidator),
    };

    const onChangeFile = (
        event: ChangeEvent,
        setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void,
     ) => {
        const target = event.target as HTMLInputElement;
        const files = target.files;
        if (files) {
            const fromData = new FormData();
            fromData.set('file',  files[0])
            axios
                .post('/media/upload', fromData)
                .then((response) => {
                    console.log(response);
                    setFieldValue(target.name, response.data.path);
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    };
    return (
        <div className='create-clinic-section'>
            <h3>Add Clinic Staff</h3>
            <div className='create-clinic content'>
                <Formik {...formik}>
                    {(formikProps) => (
                        <Form>
                            <h1>Staff Details</h1>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="firstName">First Name</label>
                                    <input
                                        type="text"
                                        id='firstName'
                                        name='firstName'
                                        className='form-control'
                                        placeholder='First Name'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="firstName"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="lastName">Last Name</label>
                                    <input
                                        type="text"
                                        id='lastName'
                                        name='lastName'
                                        className='form-control'
                                        placeholder='Last Name'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="lastName"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="email">Email</label>
                                    <input
                                        type="email"
                                        id='email'
                                        name='email'
                                        className='form-control'
                                        placeholder='Email'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="email"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="phoneNumber">Phone Number</label>
                                    <input
                                        type="text"
                                        id='phoneNumber'
                                        name='phoneNumber'
                                        className='form-control'
                                        placeholder='Phone Number'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="phoneNumber"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>
                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="licence">Licence</label>
                                    <input
                                        type="text"
                                        id='licence'
                                        name='licence'
                                        className='form-control'
                                        placeholder='Licence'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="licence"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="address2">Profile</label>
                                    <input
                                        type="file"
                                        id='profile'
                                        name='profile'
                                        className='form-control'
                                        placeholder='Profile'
                                        onChange={(event) => onChangeFile(event, formikProps.setFieldValue)}
                                    />
                                    <ErrorMessage
                                        name="profile"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>

                            <div className="form-checks">
                                <div className="form-check">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        id='password'
                                        name='password'
                                        className='form-control'
                                        placeholder='Password'
                                        onChange={formikProps.handleChange}
                                    />
                                    <ErrorMessage
                                        name="password"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                                <div className="form-check">
                                    <label htmlFor="staff">Role</label>
                                    <select
                                        name="staff"
                                        id="staff"
                                        className="form-select"
                                        // value={values.}
                                        onChange={formikProps.handleChange}
                                    >
                                        <option value='none' disabled selected>None</option>
                                        <option value='doctor'>Doctor</option>
                                        <option value='nurse'>Nurse</option>
                                        <option value='management'>Management</option>
                                        <option value='other'>Other</option>
                                    </select>
                                    <ErrorMessage
                                        name="staff"
                                        render={(error) => <span className="error">{error}</span>}
                                    />
                                </div>
                            </div>

                            <div className="submit-btn">
                                <button className="btn" type='submit'>Add Staff</button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    )
}

export default AddStaff
