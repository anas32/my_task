import React, {createContext, PureComponent, useContext} from "react";
import * as socketIOClient from 'socket.io-client';

export interface Message {
    id: string;
    message: string;
    attachments: string[];
    messageBy: string;
    room: string;
    messageTo: string;
    createdAt: string;
    read: string;
}

interface SocketMessage {
    _id: string;
    message: string;
    attachments: string[];
    messageBy: string;
    room: string;
    messageTo: string;
    createdAt: string;
    read: string;
}

interface State {
    message: Message
}

interface MessageContextInterface {
    state: State
}

export const MessagesContext = createContext<MessageContextInterface>(null);

export const useMessageContext = () => {
    return useContext(MessagesContext)
}

export class MessagesContextProvider extends PureComponent<any, State> {
    socket: SocketIOClient.Socket
    state:State = {
        message:null
    }

    componentDidMount() {
        const url = new URL("https://api.doctalklive.app/");
        url.searchParams.set('token', localStorage.getItem('access-token'));
        this.socket = socketIOClient.connect(url.href);
        this.socket.on('connect', () => {
            console.log('socketIOClient connected');
        });
        this.socket.on('disconnect', () => {
            console.log('socketIOClient disconnected');
        });
        this.socket.on(
            'message',
            (event: SocketMessage) => {
                this.setState((state) => {
                    const messageEvent = {...event, id: event._id}
                    delete messageEvent._id
                    return {
                        message: messageEvent
                    }
                })
            })
    }

    render():JSX.Element {
        const context :MessageContextInterface = {
            state: this.state
        }
        return (
            <MessagesContext.Provider value={context} >
                {this.props.children}
            </MessagesContext.Provider>
        );
    }
}


