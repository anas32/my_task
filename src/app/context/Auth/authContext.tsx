import React, { createContext, PureComponent, useContext } from 'react';
import {UserInterface} from "../../../interfaces";

interface State {
    user: UserInterface;
    isLoggedIn: boolean;
}

interface Actions {
    updateState(user: UserInterface): void;
}

export interface AuthContextInterface {
    state: State;
    actions: Actions;
}

export const AuthContext = createContext<AuthContextInterface>(null);

export const useAuthContext = () => {
    return useContext(AuthContext);
};

const getState = (): State => {
    const state: State = {
        user: null,
        isLoggedIn: false,
    };
    const user = localStorage.getItem('current-user');

    try {
        if (!!user) {
            state.user = JSON.parse(user);
            state.isLoggedIn = true;
        }
    } catch (error) {
        localStorage.clear();
        console.log(error);
    }

    return state;
};

export class AuthContextProvider extends PureComponent<any, State> {
    state: State = getState();

    updateState = (user: UserInterface): void => {
        let isLoggedIn;
        let userData: UserInterface = user;
        if (userData) {
            isLoggedIn = true;
            userData = {
                ...(this.state.user || {}),
                ...userData,
            };
            if (!userData.authToken) {
                userData.authToken = localStorage.getItem('access-token');
            }
            localStorage.setItem('current-user', JSON.stringify(userData));

            localStorage.setItem('access-token', userData.authToken);
        } else {
            isLoggedIn = false;
            localStorage.removeItem('current-user');
            localStorage.removeItem('access-token');
        }
        this.setState({
            user: userData,
            isLoggedIn,
        });
    };

    render(): JSX.Element {
        const context: AuthContextInterface = {
            state: this.state,
            actions: {
                updateState: this.updateState,
            },
        };
        return <AuthContext.Provider value={context}>{this.props.children}</AuthContext.Provider>;
    }
}
