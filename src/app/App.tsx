import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import {Router} from './Router'

function App(): JSX.Element {
  return (
      <BrowserRouter basename="/">
         <Router />
      </BrowserRouter>
  );
}

export default App;

